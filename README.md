### Dependencies
* python3
* pyserial (`pip install pyserial`)
* tkinter (comes with every python install)
* [Arduino IDE](https://www.arduino.cc/en/Main/Software)

### How to use

Upload the arduino sketch (listen.ino) to any arduino compatible device. If you're having trouble, see Arduino.cc's [HowTo](https://www.arduino.cc/en/Main/Howto). Keep the device plugged in when you run the GUI in the next step.

Run gui.py. This is as simple as `./gui.py` in MacOS/Linux/BSD. In Windows it's easy to open `gui.py` in IDLE, then Run>Run Modlule

### Screenshot

![alt text](https://gitlab.com/maxbla/serialUI/raw/master/screenshot.png)