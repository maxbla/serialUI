#!/bin/python3
import serial
from time import sleep
import tkinter as tk


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()
        self.ser = serial.Serial('/dev/ttyUSB0', timeout=5)
        self.bind("<Destroy>", self.onExit)

    def onExit(self, event):
        # print(event.type)  # debug
        self.ser.close()

    def onPress(self):
        print("input: " + self.inputText.get())
        self.ser.write(self.inputText.get().encode())
        resp = self.ser.readline(80).decode().strip()
        print("response from arduino: " + resp)
        self.outputText['text'] = "Sampling interval: " + resp

    def createWidgets(self):
        self.sendButton = tk.Button(
            self, text='Set new sapling frequency', command=self.onPress)
        self.sendButton.grid(row=0, column=0)
        self.outputText = tk.Label(
            self, text="Sampling interval: uninitalized")
        self.outputText.grid(row=1, column=0, columnspan=2)
        self.inputText = tk.StringVar()
        self.input = tk.Entry(self, textvariable=self.inputText)
        self.input.grid(row=0, column=1)


app = Application()
app.master.title('Serial Example')
app.mainloop()
