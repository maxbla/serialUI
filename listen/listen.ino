int sample_interval = -1; //how often to take a sample in minutes
const int max_line = 80;
char buf[max_line];

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  if (Serial.readBytesUntil('\n', buf, max_line) != 0)
      Serial.println(sample_interval = atoi(buf));
  digitalWrite(LED_BUILTIN, LOW);

  //if(now() - last_sample > sample_interval) {
    //take_measurement();
  //}
}
